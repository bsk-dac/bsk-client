'use strict';

const httpProxy = require('http-proxy');
const winston = require('winston');
const proxyConfig = require('./proxy-config');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/*
 * Installs routes that proxy based on the settings in ./proxy-config.
 * If no settings are provided, no proxies are installed.
 */
module.exports = (app) => {
  const paths = Object.keys(proxyConfig);
  if (!paths.length) {
    return;
  }

  const proxy = httpProxy.createProxyServer()
    .on('error', e => winston.error(e));

  proxy.on('proxyReq', function(proxyReq, req, res, options) {
    if(req.method == "POST" && req.body){
      proxyReq.write(req.body);
      proxyReq.end();
    }
  });

  paths.forEach(path => {
    const config = proxyConfig[path];
    if (path && config) {
      winston.info(`Enabling proxy ${path} => `, config);
      app.use(path, (req, res) => {

        let headers = {};
        if(req.method=="POST"&&req.body){
          let data = JSON.stringify(req.body);
          req.body = data;
          headers = {
            "Content-Type": "application/json",
            "Content-Length": data.length
          }
        }

        let configExtended = Object.assign(config, {
          headers: headers
        });

        proxy.web(req, res, configExtended);
      });
    }
  });
};
