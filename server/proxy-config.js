'use strict';

const fs = require('fs');

// Proxying to remote HTTP APIs:
//
// Proxy settings in this file are used by both the production express server
// and webpack-dev-server.
//
// In either case, the config format is that used by node-http-proxy:
// https://github.com/nodejitsu/node-http-proxy#options
//
// Note that in production it's better to either
// 1. deploy the app on the same domain as the API,
// 2. get the API to expose an x-allow-origin header, or
// 3. use a dedicated reverse proxy (e.g. Nginx) to do this instead.

console.log(require('path').dirname(require.main.filename));
console.log(require('path').dirname(require.main.filename));
console.log(require('path').dirname(require.main.filename));
console.log(require('path').dirname(require.main.filename));
console.log(require('path').dirname(__dirname ));
console.log(require('path').dirname(__dirname ));
console.log(require('path').dirname(__dirname ));
console.log(require('path').dirname(__dirname ));
console.log(require('path').dirname(__dirname ));

module.exports = {
  // Calls to /api/foo will get routed to
  // http://jsonplaceholder.typicode.com/foo.
  '/api/': {
    target: 'https://localhost:8080',
    changeOrigin: true,
    headers: {},
    secure: false,
    ssl: {
      key: fs.readFileSync(__dirname + '/graylog-pkcs5.pem', 'utf8'),
      cert: fs.readFileSync(__dirname + '/graylog-certificate.pem', 'utf8')
    }
  },
};
