import { combineReducers } from 'redux';
const { routerReducer } = require('react-router-redux');
const formReducer = require('redux-form').reducer;
import table from './table';
import session from './session';
import privileges from './privileges';

const rootReducer = combineReducers({
  session,
  table,
  privileges,
  routing: routerReducer,
  form: formReducer,
});

export default rootReducer;
