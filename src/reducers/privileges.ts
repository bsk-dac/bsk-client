import {
  PRIVILEGES_CURRENT_GET,
  PRIVILEGES_CURRENT_LOADED,
  PRIVILEGES_GET,
  PRIVILEGES_LOADED,
} from '../constants';


const INITIAL_STATE = {
  list: [],
  all: [],
  loading: true
};

function counterReducer(state = INITIAL_STATE, action = {type: '', payload: null}) {
  switch (action.type) {
    case PRIVILEGES_CURRENT_GET:
      return Object.assign({}, state, {
        list: [],
        loading: true
      });
    case PRIVILEGES_GET:
      return Object.assign({}, state, {
        all: [],
        loading: true
      });
    case PRIVILEGES_CURRENT_LOADED:
      return Object.assign({}, state, {
        list: action.payload,
        loading: false
      });
    case PRIVILEGES_LOADED:
      return Object.assign({}, state, {
        all: action.payload,
        loading: false
      });
    default:
      return state;
  }
}


export default counterReducer;
