import {
  TABLE_DATA_GET,
  TABLE_DATA_LOADED,
  TABLE_DATA_ADDED,
  TABLE_DATA_UPDATED,
} from '../constants';

// Course Payment Presence Room Customer

const INITIAL_STATE = {
  room: {
    list: [],
    loading: false
  },
  course: {
    list: [],
    loading: false
  },
  payment: {
    list: [],
    loading: false
  },
  customer: {
    list: [],
    loading: false
  },
  presence: {
    list: [],
    loading: false
  },
};

function counterReducer(state = INITIAL_STATE, action = {type: '',  payload: null, tableName: ""}) {
  let list;
  switch (action.type) {
    case TABLE_DATA_GET:
      return Object.assign({}, state, {
        [action.tableName]: {
          list: [],
          loading: true
        }
      });
    case TABLE_DATA_LOADED:
      return Object.assign({}, state, {
        [action.tableName]: {
          list: action.payload,
          loading: false
        }
      });
    case TABLE_DATA_ADDED:
      list = !!state[action.tableName] ? state[action.tableName].list.slice() : [];

      for (let i in list) {
        if (state[action.tableName].list[i].id === action.payload.oldRow.id) {
          list[i] = action.payload.newRow;
          break;
        }
      }

      return Object.assign({}, state, {
        [action.tableName]: {
          list: list,
          loading: false
        }
      });
    case TABLE_DATA_UPDATED:
      console.log("OMG", state);
      console.log("OMG", action.tableName);
      list = state[action.tableName].list.slice();

      for (let i in state[action.tableName].list) {
        if (state[action.tableName].list[i].id === action.payload.oldRow.id) {
          list[i] = action.payload.newRow;
          break;
        }
      }
      return Object.assign({}, state, {
        [action.tableName]: {
          list: list,
          loading: false
        }
      });
    default:
      return state;
  }
}


export default counterReducer;
