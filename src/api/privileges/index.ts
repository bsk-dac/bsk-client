import {get, post, postPure} from '../server/';

export function getCurrentPrivileges(userId) {
  return new Promise((resolve, reject) => {
    return get('/users/' + userId + '/privileges')
      .then((json) => {
        return resolve(json);
      })
      .then(null, (err) => reject(err));
  });
}

export function getPrivileges() {
  return new Promise((resolve, reject) => {
    return get('/users/privileges')
      .then((json) => {
        return resolve(json);
      })
      .then(null, (err) => reject(err));
  });
}

export function addNewUser(userData) {
  return new Promise((resolve, reject) => {
    return postPure('/register', userData)
      .then((json) => {
        return resolve(json);
      })
      .then(null, (err) => reject(err));
  });
}

export function passPrivilegeToUser(privilege) {
  return new Promise((resolve, reject) => {
    return postPure('/privilege/transfer', privilege)
      .then((json) => {
        return resolve(json);
      })
      .then(null, (err) => reject(err));
  });
}

export function revokePrivilegeFromUser(privilege) {
  return new Promise((resolve, reject) => {
    return postPure('/privilege/take', privilege)
      .then((json) => {
        return resolve(json);
      })
      .then(null, (err) => reject(err));
  });
}

export function passTakeUserPrivilege(privilege) {
  return new Promise((resolve, reject) => {
    return postPure('/privilege/take-over', privilege)
      .then((json) => {
        return resolve(json);
      })
      .then(null, (err) => reject(err));
  });
}
