import { get, post, del } from '../server/';

export function getTableData(tableName) {
  return new Promise((resolve, reject) => {
    return get('/' + tableName)
    .then((json) => {
      return resolve(json);
    })
    .then(null, (err) => reject(err));
  });
}

export function addTableRow(tableName, row) {
  let newRow = Object.assign({}, row);
  delete newRow.id;
  return new Promise((resolve, reject) => {
    return post('/' + tableName, newRow)
    .then((json) => {
      return resolve(json);
    })
    .then(null, (err) => reject(err));
  });
}
export function deleteTableRow(tableName, row) {
  return new Promise((resolve, reject) => {
    return del('/' + tableName + '/' + row.id, row)
    .then((json) => {
      return resolve(json);
    })
    .then(null, (err) => reject(err));
  });
}

export function updateTableRow(tableName, row) {
  return new Promise((resolve, reject) => {
    return post('/' + tableName, row)
    .then((json) => {
      return resolve(json);
    })
    .then(null, (err) => reject(err));
  });
}
