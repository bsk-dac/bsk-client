import { post } from '../server/';

const LOGIN_ERR_MSG = `
  Wprowadzone dane są niepoprawne.
`;

export function login(user) {
  return new Promise((resolve, reject) => {
    return post('/login', user)
    .then((json) => {
      const {token} = json;
      if ( !token ) {
        return reject(new Error(LOGIN_ERR_MSG));
      } else if (!!token) {
        return resolve(json);
      }
    })
    .then(null, (err) => reject(new Error(LOGIN_ERR_MSG)));
  });
}
