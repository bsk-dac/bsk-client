import 'whatwg-fetch';

export const BASE_URL = '/api';

const SESSION_DATA = JSON.parse(localStorage.getItem("typescript-react-redux-seed"));

export function post(path, data) {
  return fetch(BASE_URL + path, {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': SESSION_DATA.token
    },
    body: JSON.stringify(data)
  })
  .then(response => response.json());
}

export function get(path) {
  return fetch(BASE_URL + path, {
    method: 'get',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': SESSION_DATA.token
    }
  })
  .then(response => response.json());
}

export function put(path, data) {
  return fetch(BASE_URL + path, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': SESSION_DATA.token
    },
    body: JSON.stringify(data)
  })
    .then(response => response.json());
}

export function del(path, data) {
  return fetch(BASE_URL + path, {
    method: 'delete',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': SESSION_DATA.token
    },
    body: JSON.stringify(data)
  })
    .then(response => response.json());
}

export function postPure(path, data) {
  return fetch(BASE_URL + path, {
    method: 'post',
    headers: {
      'Accept': 'text/html',
      'Content-Type': 'application/json',
      'Authorization': SESSION_DATA.token
    },
    body: JSON.stringify(data)
  })
    .then(response => response);
}
