import * as React from 'react';
import {TableRow} from "./TableRow";
const _ = require('lodash');


export class TableComponent extends React.Component<any, any> {
  render() {
    const {onUpdateEvent, onDelEvent, rows, columnNames, onAddEvent, privileges, activeTable, onSaveEvent} = this.props;
    let i = 0;
    const rowsComponents = rows.map((row) => {
      return (
        /NEW_/ig.test(row.id) ?
          null :
          <TableRow
            onUpdateEvent={onUpdateEvent}
            row={row}
            onDelEvent={onDelEvent.bind(this)}
            onSaveEvent={onSaveEvent}
            activeTable={activeTable}
            privileges={privileges}
            key={++i}/>
      );
    });
    let rowsAdd = [];
    _.forIn(rows, (row) => {
      if (/NEW_/ig.test(row.id)) {
        rowsAdd.push(
          <TableRow
            onUpdateEvent={onUpdateEvent}
            row={row}
            onDelEvent={onDelEvent.bind(this)}
            onSaveEvent={onSaveEvent}
            activeTable={activeTable}
            privileges={privileges}
            key={row.id}/>
        );
      }
    });
    const columnComponents = columnNames.map((columnName) => {
      return (
        <th key={columnName}>{columnName}</th>
      );
    });
    const hasInsert = privileges.indexOf('I') !== -1 || privileges.indexOf('i') !== -1;
    const hasSelect = privileges.indexOf('S') !== -1 || privileges.indexOf('s') !== -1;

    let component;
    let addRowContainer = rowsAdd.length ?
      <table style={{margin: "0 auto"}} className="table table-bordered">
        <thead>
        <tr>
          {columnComponents}
        </tr>
        </thead>
        <tbody>
        {rowsAdd}
        </tbody>
      </table> : null;

    if (hasSelect) {
      if (rows.length) {
        component =
          <table style={{margin: "0 auto"}} className="table table-bordered">
            <thead>
            <tr>
              {columnComponents}
            </tr>
            </thead>
            <tbody>
            {rowsComponents}
            </tbody>
          </table>;
      } else {
        component =
          <h3>Tabela jest pusta.</h3>;
      }
    } else {
      component = <h3>Brak uprawnień do odczytu.</h3>;
    }

    return (
      <div>
        {component}
        {addRowContainer}
        {
          hasInsert ?
            <div style={{margin: "0 auto", width: "220px"}}>
              <button style={{margin: "20px"}} type="button" onClick={onAddEvent} className="button">Dodaj nowy wiersz
              </button>
            </div>
            : null
        }
      </div>
    );

  }

}
