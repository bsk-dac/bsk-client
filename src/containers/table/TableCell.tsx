import * as React from 'react';

export class TableCell extends React.Component<any, any> {
  render() {
    // console.log("CELL", this.props.cellData);
    return (
      <td>
        <input
          disabled={this.props.cellData.name === "id"}
          type='text'
          name={this.props.cellData.name}
          id={this.props.cellData.id}
          value={this.props.cellData.name === "id" && /NEW_/ig.test(this.props.cellData.value) || this.props.cellData.value == null ? "" : this.props.cellData.value}
          onChange={this.props.onUpdateEvent.bind(this)}
        />
      </td>
    );
  }
}
