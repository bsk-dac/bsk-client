import * as React from 'react';
import {TableCell} from "./TableCell";
const _ = require('lodash');

export class TableRow extends React.Component<any, any> {
  onDelEvent() {
    this.props.onDelEvent(this.props.row);
  }
  onSaveEvent() {
    this.props.onSaveEvent(this.props.row);
  }

  render() {
    const {row, onUpdateEvent, privileges} = this.props;
    const hasDelete = privileges.indexOf('D') !== -1 || privileges.indexOf('d') !== -1;
    const hasUpdate = privileges.indexOf('U') !== -1 || privileges.indexOf('u') !== -1;
    const hasInsert = privileges.indexOf('I') !== -1 || privileges.indexOf('i') !== -1;

    // console.log("privileges delete", hasDelete);
    // console.log("privileges update", hasUpdate);
    const rowComponent = _.map(row, (value, name) => {
      return (
        hasUpdate || /NEW_/ig.test(row.id) ?
          <TableCell
            onUpdateEvent={onUpdateEvent}
            cellData={{
              name: name,
              value: value,
              id: row.id
            }}
            key={name}/> : <td>{value}</td>
      );
    });

    const addButton =
      hasInsert && /NEW_/ig.test(row.id) ?
        <td className="del-cell">
          <button type="button" onClick={this.onSaveEvent.bind(this)} className="button">Zapisz</button>
        </td> : null;

    return (
      <tr className="eachRow">
        {rowComponent}
        {hasDelete ?
          <td className="del-cell">
            <input type="button" onClick={this.onDelEvent.bind(this)} value="X" className="del-btn"/>
          </td> : null}
        {addButton}
      </tr>
    );

  }
}
