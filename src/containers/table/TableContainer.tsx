import * as React from 'react';
import {TableComponent} from "./TableComponent";
const _ = require('lodash');

export class TableContainer extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      rows: this.props.rows
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      rows: nextProps.rows
    });
  }

  onDelEvent(row) {
    const index = this.state.rows.indexOf(row);
    this.state.rows.splice(index, 1);
    this.setState({
      rows: this.state.rows
    });
    // @todo CALL API to REMOVE
    if (/NEW_/ig.test(row.id)) {
      console.log("delete local", row);
    } else {
      // update
      console.log("delete from server", row);
      this.props.deleteRow(this.props.activeTable, row);
    }
  };

  onAddEvent(e) {
    let row = {};
    _.map(this.props.columnNames, (name) => row[name] = "");
    row["id"] = "NEW_" + (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    this.state.rows.push(row);
    console.log("onAddEvent", this.state.rows);
    this.setState({
      rows: this.state.rows
    });
  }

  onUpdateEvent(e) {
    const newCell = {
      id: e.target.id,
      value: e.target.value,
      name: e.target.name
    };
    let newRow = {};
    const {addRow, updateRow, activeTable} = this.props;

    const rows = this.state.rows;
    const newRows = rows.map((row) => {
      for (let key in row) {
        if (key === newCell.name && row.id == newCell.id) {
          row[key] = newCell.value;
          newRow = Object.assign({}, row);
        }
      }
      return row;
    });
    this.setState({
      rows: newRows
    });

    // @todo CALL API to update/add
    if (/NEW_/ig.test(newCell.id)) {
      // addRow(this.props.activeTable, newRow);
    } else {
      updateRow(activeTable, newRow);
      // update
      console.log("update", newCell, newRow);
    }
  };

  onSaveEvent(row) {
    const {addRow} = this.props;
    // @todo CALL API to update/add
    if (/NEW_/ig.test(row.id)) {
      addRow(this.props.activeTable, row);
      console.log("SAVE", row);
    }
  };

  render() {
    const {privileges, activeTable} = this.props;
    return (
      <TableComponent
        onUpdateEvent={this.onUpdateEvent.bind(this)}
        onAddEvent={this.onAddEvent.bind(this)}
        onDelEvent={this.onDelEvent.bind(this)}
        onSaveEvent={this.onSaveEvent.bind(this)}
        rows={this.state.rows}
        activeTable={activeTable}
        privileges={privileges}
        columnNames={this.props.columnNames}/>
    );

  }

}
