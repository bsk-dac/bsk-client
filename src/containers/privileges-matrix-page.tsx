import * as React from 'react';
const connect = require('react-redux').connect;
import {getAllPrivileges, addUser, passTakePrivilege} from '../actions/privileges';
import has = Reflect.has;
const _ = require('lodash');

function mapStateToProps(state) {
  return {
    allUserPrivileges: state.privileges.all,
    ownPrivileges: state.privileges.list,
    session: state.session
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllPrivileges: (): void => dispatch(getAllPrivileges()),
    addUser: (userData): void => dispatch(addUser(userData)),
    passTakePrivilege: (userData): void => dispatch(passTakePrivilege(userData)),
  };
}

class PrivilegesMatrixPage extends React.Component<any, any> {

  private user;

  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: "",
        surname: "",
        login: "",
        password: "",
      },
      error: false,
      passError: -1,
    };
  }

  componentDidMount() {
    this.props.getAllPrivileges();
  }

  onChange(e) {
    const {data, error, passError} = this.state;
    let newData = data;
    newData[e.target.name] = e.target.value;
    this.setState({
      data: newData,
      error: error,
      passError: passError,
    });
  };

  onClick() {
    const {data, passError} = this.state;
    let hasError = false;

    _.map(data, (input) => {
      if (!input.trim()) {
        hasError = true;
      }
    });

    if (!hasError) {
      this.props.addUser(data).then(() => {
        this.setState({
          data: {
            name: "",
            surname: "",
            login: "",
            password: "",
          },
          error: hasError,
          passError: passError,
        });
        this.props.getAllPrivileges();
      });
    } else {
      this.setState({
        data: data,
        error: hasError
      });
    }
  }

  onPassClick() {
    this.setState(Object.assign({}, this.state, {
      passError: -1
    }));
    this.props.passTakePrivilege({
      login: this.user.value
    }).then((response) => {
      if (403 === response.status) {
        this.setState(Object.assign({}, this.state, {
          passError: 1
        }));
      } else if (200 === response.status) {
        this.setState(Object.assign({}, this.state, {
          passError: 0
        }));
        this.props.getAllPrivileges();
      }
      // console.log("response", response);
    });
    // console.log("pass", this.user.value);
  }

  render() {
    const {session} = this.props;
    const currentUser = session.get('userData', false).get('login') || "";
    const rows = _.map(this.props.allUserPrivileges, (privileges, user) => {
      return (
        <tr key={user}>
          <td>{user}</td>
          <td>{privileges["przejmij"] || "-"}</td>
          <td>{privileges["course"] || "-"}</td>
          <td>{privileges["instructor"] || "-"}</td>
          <td>{privileges["room"] || "-"}</td>
          <td>{privileges["customer"] || "-"}</td>
        </tr>
      );
    });

    let usersOptions = [];
    _.forIn(this.props.allUserPrivileges, (privileges, user)  => {
      if (currentUser != user) {
        usersOptions.push(<option key={user} value={user}>{user}</option>);
      }
    });

    const passForm = !!this.props.ownPrivileges["admin"] ?
      <div style={{float: "left"}}>
        {1 === this.state.passError ?
          <div style={{width: "400px", margin: "0 auto"}} className="alert callout">
            Operacja nie powiodła się.
          </div> : null}
        {0 === this.state.passError ?
          <div style={{width: "400px", margin: "0px auto 16px auto"}} className="success callout">
            Operacja powiodła się.
          </div> : null}
        <span>Uprawnienie do przejmowania uprawnień</span>
        <table style={{width: "400px"}} className="table table-bordered">
          <tbody>
          <tr>
            <td>
              <select ref={(input) => {
                this.user = input;
              }} name="user">
                {usersOptions}
              </select>
            </td>
            <td>
              <button type="button" onClick={this.onPassClick.bind(this)} className="button">Nadaj</button>
            </td>
          </tr>
          </tbody>
        </table>
      </div> : null;

    const usersForm =
      <div style={{width: "300px", float: "right"}}>
        <h3>Dodaj użytkownika</h3>
        {this.state.error ?
          <div style={{width: "300px", margin: "0 auto"}} className="alert callout">
            Wszystkie pola formularza są wymagane.
          </div> : null}
        <form style={{width: "300px", margin: "0 auto"}}>
          <div>
            <label>Imię
              <input onChange={this.onChange.bind(this)} value={this.state.data.name} type="text" placeholder="Imię"
                     name="name"/>
            </label>
          </div>
          <div>
            <label>Nazwisko
              <input onChange={this.onChange.bind(this)} value={this.state.data.surname} type="text"
                     placeholder="Nazwisko" name="surname"/>
            </label>
          </div>
          <div>
            <label>Login
              <input onChange={this.onChange.bind(this)} value={this.state.data.login} type="text" placeholder="Login"
                     name="login"/>
            </label>
          </div>
          <div>
            <label>Password
              <input onChange={this.onChange.bind(this)} value={this.state.data.password} type="password"
                     placeholder="Password" name="password"/>
            </label>
          </div>
          <div style={{margin: "0 auto", width: "90px"}}>
            <button style={{margin: "20px"}} type="button" onClick={this.onClick.bind(this)} className="button">Dodaj
            </button>
          </div>
        </form>
      </div>;

    return (
      <div>
        <h1>Macierz uprawnień/użytkownicy</h1>
        <div style={{margin: "0 auto", width: "900px"}}>
          {passForm}
          <table style={{float: "left", width: "500px"}} className="table table-bordered">
            <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Uprawnienie: <i>Przejmij</i></th>
              <th>Tabela: <i>Course</i></th>
              <th>Tabela: <i>Instruktor</i></th>
              <th>Tabela: <i>Room</i></th>
              <th>Tabela: <i>Customer</i></th>
            </tr>
            </thead>
            <tbody>
            {rows}
            </tbody>
          </table>
          {usersForm}
        </div>
      </div>
    );
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivilegesMatrixPage);
