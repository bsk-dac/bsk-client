import * as React from 'react';
const connect = require('react-redux').connect;
const Link = require('react-router').Link;

import { loginUser, logoutUser } from '../actions/session';
import {getUserPrivileges} from '../actions/privileges';
import Button from '../components/button';
import Content from '../components/content';
import LoginModal from '../components/login/login-modal';
import Logo from '../components/logo';
import Navigator from '../components/navigator';
import NavigatorItem from '../components/navigator-item';

interface IAppProps extends React.Props<any> {
  session: any;
  login: () => void;
  logout: () => void;
};

function mapStateToProps(state) {
  return {
    session: state.session,
    router: state.routing,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: () => dispatch(loginUser()),
    logout: () => dispatch(logoutUser()),
    getUserPrivileges: (userId) => dispatch(getUserPrivileges(userId)),
  };
}

class App extends React.Component<any, void> {

  getPriviledges() {
    const {session} = this.props;
    const token = session.get('token', false);
    const isLoggedIn = token && token !== null && typeof token !== 'undefined';
    if (isLoggedIn) {
      const id = session.get('userData', false).get('id');
      this.props.getUserPrivileges(id);
    }
  }

  componentDidMount() {
    this.getPriviledges();
  }

  componentWillReceiveProps() {
    this.getPriviledges();
  }

  render() {
    const { children, session, login, logout } = this.props;
    const token = session.get('token', false);
    const isLoggedIn = token && token !== null && typeof token !== 'undefined';
    const location = this.props.router.locationBeforeTransitions.pathname;
    this.getPriviledges();
    const style = {
      fontWeight: "bold",
    };

    return (
      <div>
        <LoginModal
          onSubmit={ login }
          isPending={ session.get('isLoading', false) }
          hasError={ session.get('hasError', false) }
          isVisible={ !isLoggedIn } />
        <Navigator testid="navigator">
          <NavigatorItem mr>
            <Logo />
          </NavigatorItem>
          <NavigatorItem isVisible={ isLoggedIn } mr>
            <Link style={"/" == location ? style : {}} to="/">Strona główna</Link>
          </NavigatorItem>
          <NavigatorItem isVisible={ isLoggedIn } mr>
            <Link style={"/tables" == location ? style : {}} to="/tables">Tabele</Link>
          </NavigatorItem>
          <NavigatorItem isVisible={ isLoggedIn } mr>
            <Link style={"/pass" == location ? style : {}} to="/pass">Nadawanie uprawnień</Link>
          </NavigatorItem>
          <NavigatorItem isVisible={ isLoggedIn } mr>
            <Link style={"/revoke" == location ? style : {}} to="/revoke">Odbieranie uprawnień</Link>
          </NavigatorItem>
          <NavigatorItem isVisible={ isLoggedIn } mr>
            <Link style={"/matrix" == location ? style : {}} to="/matrix">Macierz uprawnień</Link>
          </NavigatorItem>
          <div className="flex flex-auto"></div>
          <NavigatorItem isVisible={ isLoggedIn }>
            { isLoggedIn ?
              <span>Witaj <b>{session.get('userData', false).get('name')} {session.get('userData', false).get('surname')}</b>!&nbsp;&nbsp;</span>
              : null }
            <Button onClick={ logout } className="bg-red white">
              Wyloguj
            </Button>
          </NavigatorItem>
        </Navigator>
        <Content isVisible={ isLoggedIn }>
          { children }
        </Content>
      </div>
    );
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
