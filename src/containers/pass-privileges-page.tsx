import * as React from 'react';
const connect = require('react-redux').connect;
import {getAllPrivileges, passPrivilege} from '../actions/privileges';
import has = Reflect.has;
const _ = require('lodash');

function mapStateToProps(state) {
  return {
    allUserPrivileges: state.privileges.all,
    ownPrivileges: state.privileges.list,
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllPrivileges: (): void => dispatch(getAllPrivileges()),
    passPrivilege: (privilege): void => dispatch(passPrivilege(privilege))
  };
}

class PassPrivilegesPage extends React.Component<any, any> {

  private for;
  private privilege;
  private user;

  constructor(props) {
    super(props);
    this.state = {
      error: -1,
      selectedTable: "",
    };
  }

  componentDidMount() {
    this.props.getAllPrivileges();
  }

  onTableChange() {
    const {error} = this.state;
    this.setState({
      error: error,
      selectedTable: this.for.value
    });
  }

  onClick() {
    const {selectedTable} = this.state;
    const request = {
      "recipient": this.user.value,
      "table": this.for.value,
      "privileges": [this.privilege.value]
    };
    this.setState({
      error: -1,
      selectedTable
    });
    this.props.passPrivilege(request)
      .then((response) => {
        if (403 === response.status) {
          this.setState({
            error: 1,
            selectedTable
          });
        } else if (200 === response.status) {
          this.setState({
            error: 0,
            selectedTable
          });
        }
      });
  }

  render() {
    const {session} = this.props;
    const usersOptions = [];
    const privilegesOptions = [];
    const usersTables = [];
    const selectedTable = this.for ? this.for.value : "";
    const currentUser = session.get('userData', false).get('login') || "";
    const privilegesMap = {
      S: "s",
      I: "i",
      U: "u",
      D: "d",
    };

    _.forIn(this.props.ownPrivileges, (privileges, tableName) => {
      if ("admin" !== tableName && "przejmij" !== tableName) {
        usersTables.push(<option key={tableName} value={tableName}>{tableName.charAt(0).toUpperCase() + tableName.slice(1)}</option>);
      }
    });

    if (!!this.props.ownPrivileges[selectedTable]) {
      let i = 0;
      _.forIn(this.props.ownPrivileges[selectedTable], (privilege, key) => {
        privilegesOptions.push(<option key={++i} value={privilege}>{privilege}</option>);
        if (!!privilegesMap[privilege]) {
          privilegesOptions.push(<option key={++i} value={privilegesMap[privilege]}>{privilegesMap[privilege]}</option>);
        }
      });
    }

    _.forIn(this.props.allUserPrivileges, (privilege, user) => {
      if (currentUser != user) {
        usersOptions.push(<option key={user} value={user}>{user}</option>);
      }
    });

    return (
      <div>
        <h1>Nadawanie uprawnień</h1>
        {1 === this.state.error ?
          <div style={{width: "400px", margin: "0px auto 16px auto"}} className="alert callout">
            Operacja nie może zostać wykonana.
          </div> : null}
        {0 === this.state.error ?
          <div style={{width: "400px", margin: "0px auto 16px auto"}} className="success callout">
            Operacja powiodła się.
          </div> : null}
        <table style={{margin: "0 auto", width: "600px"}} className="table table-bordered">
          <thead>
          <tr>
            <th>Tabela</th>
            <th>Uprawnienie</th>
            <th>Komu</th>
            <th>Akcja</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <select onChange={this.onTableChange.bind(this)} ref={(input) => {
                this.for = input;
              }} name="for">
                {usersTables}
              </select>
            </td>
            <td>
              <select ref={(input) => {
                this.privilege = input;
              }} name="privilege">
                {privilegesOptions}
              </select>
            </td>
            <td>
              <select ref={(input) => {
                this.user = input;
              }} name="user">
                {usersOptions}
              </select>
            </td>
            <td>
              <button type="button" onClick={this.onClick.bind(this)} className="button">Przekaż</button>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    );
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PassPrivilegesPage);
