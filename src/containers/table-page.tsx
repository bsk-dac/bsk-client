import * as React from 'react';
const connect = require('react-redux').connect;
import Container from '../components/container';
import {getData, addRow, updateRow, deleteRow} from '../actions/table';
import {TableContainer} from "./table/TableContainer";
import Navigator from '../components/navigator';
import NavigatorItem from '../components/navigator-item';
const _ = require('lodash');

function mapStateToProps(state) {
  return {
    rows: state.table,
    privileges: state.privileges.list
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getData: (tableName): void => dispatch(getData(tableName)),
    addRow: (tableName, row): void => dispatch(addRow(tableName, row)),
    updateRow: (tableName, row): void => dispatch(updateRow(tableName, row)),
    deleteRow: (tableName, row): void => dispatch(deleteRow(tableName, row))
  };
}

class TablePage extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      activeTable: ""
    };
  }

  setActiveTable(name) {
    const {privileges = []} = this.props;
    this.setState({
      activeTable: name
    });
    const tablePrivileges = privileges[name] || [];
    const hasSelect = tablePrivileges.length ? (tablePrivileges.indexOf('S') !== -1 || tablePrivileges.indexOf('s') !== -1) : false;
    if (hasSelect) {
      this.props.getData(name);
    }
  }

  render() {
    const {rows, addRow, updateRow, deleteRow, privileges = []} = this.props;
    const tablePrivilegessss = privileges[this.state.activeTable] || [];
    const hasSelect = tablePrivilegessss.length ? (tablePrivilegessss.indexOf('S') !== -1 || tablePrivilegessss.indexOf('s') !== -1) : false;
    let tableData = [];
    if (hasSelect) {
      tableData = this.state.activeTable ? rows[this.state.activeTable].list : [];
    } else {
      tableData = [];
    }

    const hasData = tableData.length;
    let columnNames;
    switch (this.state.activeTable) {
      case "room":
        columnNames = ["id", "number", "size", "floor"];
        break;
      case "instructor":
        columnNames = ["id", "name", "surname", "experience", "empmlyment_period"];
        break;
      case "course":
        columnNames = ["id", "name", "startDate", "endDate", "category"];
        break;
      case "customer":
        columnNames = ["id", "name", "surname", "address", "phone"];
        break;
    }

    let tables = Object.keys(privileges);

    let filteredTables = [];
    _.forIn(tables, (tableName) => {
      if ("admin" !== tableName && "przejmij" !== tableName && (privileges[tableName].indexOf('S') !== -1 || privileges[tableName].indexOf('s') !== -1)) {
        filteredTables.push(tableName);
      }
    });

    const navigation = _.map(filteredTables, (name, key) => {
      let style;
      if (name == this.state.activeTable) {
        style = {
          cursor: "pointer",
          fontWeight: "bold",
        };
      } else {
        style = {cursor: "pointer"};
      }
      return (
        <NavigatorItem key={key} mr>
          <a style={style} onClick={() => {
            this.setActiveTable(name);
          }}>{name.charAt(0).toUpperCase() + name.slice(1)}</a>
        </NavigatorItem>
      );
    });
    const tablePrivileges = privileges[this.state.activeTable] || [];

    return (
      filteredTables.length ?
        <Container testid="counter" size={6} center>
          <Navigator testid="navigator">
            {navigation}
          </Navigator>
          {!this.state.activeTable ?
            <h3>Wybierz tabelę z menu</h3> :
            <div>
              <h1
                className='center caps'
                id='qa-counter-heading'>
                {this.state.activeTable.charAt(0).toUpperCase() + this.state.activeTable.slice(1)}
              </h1>
              <TableContainer
                rows={tableData}
                addRow={addRow}
                updateRow={updateRow}
                deleteRow={deleteRow}
                activeTable={this.state.activeTable}
                privileges={tablePrivileges}
                columnNames={columnNames}/>
            </div> }
        </Container> : <h3>Brak uprawnień do jakiejkolwiek z tabel!</h3>
    );
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TablePage);
