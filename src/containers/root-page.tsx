import * as React from 'react';

export default class RootPage extends React.Component<any, void> {

  render() {
    return (
      <div>
        <h1>DAC</h1>
        <ul style={{margin: "0 auto", width: "700px"}}>
          <li>Tabele - lista dostępnych tabel</li>
          <li>Przekazywanie uprawnień - mechanizm przekazywania</li>
          <li>Odbieranie uprawnień - mechanizm odbierania</li>
          <li>Macierz uprawnień - wyświetlenie użytkowników oraz posiadanych przez nich uprawnień/dodawanie użytkowników</li>
        </ul>
      </div>
    );
  };
}
