import * as React from 'react';
const ReduxForm = require('redux-form');
const connect = require('react-redux').connect;
import {addRow} from "../../actions/table";

import Form from '../form/';
import FormGroup from '../form/form-group';
import FormLabel from '../form/form-label';
import FormError from '../form/form-error';
import Input from '../form/form-input';
import Button from '../button';
import Alert from '../alert';

class Row extends React.Component<any, void> {
  render() {
    const {
      handleSubmit,
      hasError,
      fields: {
        room_number,
        room_size,
        room_floor,
      }
    } = this.props;

    return (
      <Form handleSubmit={ handleSubmit }>
        <Alert
          testid="alert-error"
          id="qa-alert"
          isVisible={ hasError }
          status="error">
          Wprowadzone dane są niepoprawne
        </Alert>

        <FormGroup>
          <FormLabel id="qa-rn-label">Room number</FormLabel>
          <Input
            type="text" fieldDefinition={ room_number }
            id="qa-rn-input"
            placeholder="Room number"/>
          <FormError id="qa-rn-validation"
            isVisible={ !!(room_number.touched && room_number.error) }>
            { room_number.error }
          </FormError>

          <FormLabel id="qa-rs-label">Room size</FormLabel>
          <Input
            type="text" fieldDefinition={ room_size }
            id="qa-rs-input"
            placeholder="Room size"/>
          <FormError id="qa-rn-validation"
            isVisible={ !!(room_size.touched && room_size.error) }>
            { room_size.error }
          </FormError>

          <FormLabel id="qa-rs-label">Room floor</FormLabel>
          <Input
            type="text" fieldDefinition={ room_floor }
            id="qa-rf-input"
            placeholder="Room floor"/>
          <FormError id="qa-rf-validation"
            isVisible={ !!(room_floor.touched && room_floor.error) }>
            { room_floor.error }
          </FormError>
        </FormGroup>

        <FormGroup>
          <Button type="submit" className="mr1" id="qa-row-button">
            Dodaj
          </Button>
        </FormGroup>
      </Form>
    );
  }

  static validate(values) {
    const errors = { room_number: '', room_floor: '', room_size: '' };

    if (!values.room_number) {
      errors.room_number = 'Numer pokoju jest wymagany.';
    }
    if (!values.room_floor) {
      errors.room_floor = 'Numer piętra jest wymagany.';
    }
    if (!values.room_size) {
      errors.room_size = 'Rozmiar pokoju jest wymagany.';
    }

    return errors;
  }
}
function mapDispatchToProps(dispatch) {
  return {
    handleSubmit: (): void => dispatch(addRow()),
  };
}

const wrapper = connect(
  null,
  mapDispatchToProps
)(Row);

export default ReduxForm.reduxForm({
  form: 'row',
  fields: [
    'room_number',
    'room_floor',
    'room_size',
  ],
  validate: Row.validate,
})(wrapper);

