import * as React from 'react';
const LogoImage = require('../../assets/logo.png');

const styles = { width: 64 };

export default function Logo() {
  return (
    <div className="flex items-center">
      <img style={ styles }
        src={ LogoImage }
        alt="BSK" />
    </div>
  );
}
