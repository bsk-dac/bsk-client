import * as React from 'react';
const { IndexRoute, Route } = require('react-router');

import App from '../containers/app';
import TablePage from '../containers/table-page';
import RootPage from '../containers/root-page';
import PassPrivilegesPage from '../containers/pass-privileges-page';
import RevokePrivilegesPage from '../containers/revoke-privileges-page';
import PrivilegesMatrixPage from '../containers/privileges-matrix-page';


export default (
  <Route path="/" component={ App }>
    <IndexRoute component={ RootPage }/>
    <Route path="tables" component={TablePage}/>
    <Route path="pass" component={PassPrivilegesPage}/>
    <Route path="revoke" component={RevokePrivilegesPage}/>
    <Route path="matrix" component={PrivilegesMatrixPage}/>
  </Route>
);
