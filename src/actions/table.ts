import {getTableData, addTableRow, updateTableRow, deleteTableRow} from '../api/tables';
import {
  TABLE_DATA_LOADED,
  TABLE_DATA_ADDED,
  TABLE_DATA_GET,
  TABLE_DATA_UPDATED,
} from '../constants';

export function getData(tableName) {
  return (dispatch) => {
    getTableData(tableName)
      .then((res) => {
        dispatch({
          type: TABLE_DATA_LOADED,
          tableName: tableName,
          payload: res
        });
        return res;
      });

    return dispatch({
      type: TABLE_DATA_GET,
      tableName: tableName
    });
  };
}

export function addRow(tableName, row) {
  return (dispatch) => {
    return addTableRow(tableName, row)
      .then((res) => {
        dispatch({
          type: TABLE_DATA_ADDED,
          tableName: tableName,
          payload: {
            newRow: res,
            oldRow: row
          }
        });
        return res;
      });
  };
}

export function updateRow(tableName, row) {
  return (dispatch) => {
    return updateTableRow(tableName, row)
      .then((res) => {
        dispatch({
          type: TABLE_DATA_UPDATED,
          tableName: tableName,
          payload: {
            newRow: res,
            oldRow: row
          }
        });
        return res;
      });
  };
}

export function deleteRow(tableName, row) {
  return (dispatch) => {
    return deleteTableRow(tableName, row)
      .then((res) => {
        return res;
      });
  };
}
