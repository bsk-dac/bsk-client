import {getCurrentPrivileges, getPrivileges, addNewUser, passPrivilegeToUser, revokePrivilegeFromUser, passTakeUserPrivilege} from '../api/privileges';
import {
  PRIVILEGES_CURRENT_GET,
  PRIVILEGES_CURRENT_LOADED,
  PRIVILEGES_GET,
  PRIVILEGES_LOADED,
  PRIVILEGES_USER_ADDED,
  PRIVILEGES_PASSED,
  PRIVILEGES_REVOKED,
  PRIVILEGES_TAKE_PASSED,
} from '../constants';

export function getUserPrivileges(userId) {
  return (dispatch) => {
    getCurrentPrivileges(userId)
      .then((res) => {
        dispatch({
          type: PRIVILEGES_CURRENT_LOADED,
          payload: res
        });
        return res;
      });

    return dispatch({
      type: PRIVILEGES_CURRENT_GET
    });
  };
}
// privileges
export function getAllPrivileges() {
  return (dispatch) => {
    getPrivileges()
      .then((res) => {
        dispatch({
          type: PRIVILEGES_LOADED,
          payload: res
        });
        return res;
      });

    return dispatch({
      type: PRIVILEGES_GET
    });
  };
}

export function addUser(userData) {
  return (dispatch) => {
    return addNewUser(userData)
      .then((res) => {
        dispatch({
          type: PRIVILEGES_USER_ADDED,
          payload: res
        });
        return res;
      });
  };
}

export function passPrivilege(privilege) {
  return (dispatch) => {
    return passPrivilegeToUser(privilege)
      .then((res) => {
        dispatch({
          type: PRIVILEGES_PASSED,
          payload: res
        });
        return res;
      });
  };
}

export function revokePrivilege(privilege) {
  return (dispatch) => {
    return revokePrivilegeFromUser(privilege)
      .then((res) => {
        dispatch({
          type: PRIVILEGES_REVOKED,
          payload: res
        });
        return res;
      });
  };
}

export function passTakePrivilege(privilege) {
  return (dispatch) => {
    return passTakeUserPrivilege(privilege)
      .then((res) => {
        dispatch({
          type: PRIVILEGES_TAKE_PASSED,
          payload: res
        });
        return res;
      });
  };
}
